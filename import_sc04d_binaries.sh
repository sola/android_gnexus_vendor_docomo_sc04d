#!/bin/sh
if [ $# -ne 2 ]; then
   echo "example usage: $0 \$ANDROID_ROOT ~/sc04d_binaries"
   exit 1
fi

JCROM_ANDROID_ROOT=$1
JCROM_SC04D_BIN=$2

mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/updatecmds
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/permissions
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/fonts
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/framework
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/bin
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/etc
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/firmware
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/drm
mkdir -p ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/hw

cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/spn-conf.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/spn-conf.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/updatecmds/google_generic_update.txt ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/updatecmds/google_generic_update.txt
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/permissions/com.google.android.media.effects.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/permissions/com.google.android.media.effects.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/permissions/features.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/permissions/features.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/permissions/com.google.android.maps.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/permissions/com.google.android.maps.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/permissions/com.samsung.device.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/permissions/com.samsung.device.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/etc/voicemail-conf.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/etc/voicemail-conf.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/fonts/MTLmr3m.ttf ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/fonts/MTLmr3m.ttf
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/bootanimation.zip ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/bootanimation.zip
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/PFFprec_600.emd ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/PFFprec_600.emd
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/LMprec_508.emd ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/LMprec_508.emd
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/video/Sunset.240p.mp4 ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video/Sunset.240p.mp4
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/video/AndroidInSpace.240p.mp4 ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video/AndroidInSpace.240p.mp4
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/video/AndroidInSpace.480p.mp4 ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video/AndroidInSpace.480p.mp4
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/video/Sunset.480p.mp4 ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video/Sunset.480p.mp4
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/video/Disco.240p.mp4 ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video/Disco.240p.mp4
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/media/video/Disco.480p.mp4 ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/media/video/Disco.480p.mp4
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/framework/com.google.android.media.effects.jar ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/framework/com.google.android.media.effects.jar
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/framework/com.google.android.maps.jar ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/framework/com.google.android.maps.jar
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/framework/com.google.widevine.software.drm.jar ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/framework/com.google.widevine.software.drm.jar
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/framework/com.samsung.device.jar ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/framework/com.samsung.device.jar
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GalleryGoogle/GalleryGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GalleryGoogle/GalleryGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GalleryGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GalleryGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GenieWidget/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GenieWidget/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GenieWidget/GenieWidget.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GenieWidget/GenieWidget.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/LatinImeDictionaryPack/LatinImeDictionaryPack.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/LatinImeDictionaryPack/LatinImeDictionaryPack.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/LatinImeDictionaryPack/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/LatinImeDictionaryPack/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleLoginService/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleLoginService/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleLoginService/GoogleLoginService.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleLoginService/GoogleLoginService.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/FaceLock/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/FaceLock/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/FaceLock/FaceLock.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/FaceLock/FaceLock.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GMS_Maps/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GMS_Maps/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GMS_Maps/GMS_Maps.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GMS_Maps/GMS_Maps.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/VideoEditorGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/VideoEditorGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/VideoEditorGoogle/VideoEditorGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/VideoEditorGoogle/VideoEditorGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/CalendarGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/CalendarGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/CalendarGoogle/CalendarGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/CalendarGoogle/CalendarGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/OneTimeInitializer/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/OneTimeInitializer/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/OneTimeInitializer/OneTimeInitializer.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/OneTimeInitializer/OneTimeInitializer.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/SetupWizard/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/SetupWizard/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/SetupWizard/SetupWizard.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/SetupWizard/SetupWizard.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/talkback/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/talkback/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/talkback/talkback.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/talkback/talkback.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleContactsSyncAdapter/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleContactsSyncAdapter/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/TagGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/TagGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/TagGoogle/TagGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/TagGoogle/TagGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleEarth/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleEarth/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleEarth/GoogleEarth.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleEarth/GoogleEarth.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Music2/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Music2/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Music2/Music2.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Music2/Music2.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/ChromeBookmarksSyncAdapter/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/ChromeBookmarksSyncAdapter/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/ChromeBookmarksSyncAdapter/ChromeBookmarksSyncAdapter.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/ChromeBookmarksSyncAdapter/ChromeBookmarksSyncAdapter.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleFeedback/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleFeedback/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleFeedback/GoogleFeedback.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleFeedback/GoogleFeedback.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/MediaUploader/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/MediaUploader/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/MediaUploader/MediaUploader.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/MediaUploader/MediaUploader.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Talk/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Talk/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Talk/Talk.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Talk/Talk.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/LatinImeGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/LatinImeGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/LatinImeGoogle/LatinImeGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/LatinImeGoogle/LatinImeGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/iWnnIME2_No_Flick/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/iWnnIME2_No_Flick/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/iWnnIME2_No_Flick/iWnnIME2_No_Flick.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/iWnnIME2_No_Flick/iWnnIME2_No_Flick.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleQuickSearchBox/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleQuickSearchBox/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleQuickSearchBox/GoogleQuickSearchBox.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleQuickSearchBox/GoogleQuickSearchBox.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/VoiceSearch/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/VoiceSearch/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/VoiceSearch/VoiceSearch.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/VoiceSearch/VoiceSearch.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Gmail/Gmail.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Gmail/Gmail.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Gmail/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Gmail/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/CellBroadcastReceiver/CellBroadcastReceiver.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/CellBroadcastReceiver/CellBroadcastReceiver.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/CellBroadcastReceiver/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/CellBroadcastReceiver/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/MiniDcmWapPushHelper/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/MiniDcmWapPushHelper/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/MiniDcmWapPushHelper/MiniDcmWapPushHelper.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/MiniDcmWapPushHelper/MiniDcmWapPushHelper.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/spmode_mail_downloader/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/spmode_mail_downloader/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/spmode_mail_downloader/spmode_mail_downloader.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/spmode_mail_downloader/spmode_mail_downloader.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/ExchangeGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/ExchangeGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/ExchangeGoogle/ExchangeGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/ExchangeGoogle/ExchangeGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Thinkfree/Thinkfree.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Thinkfree/Thinkfree.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Thinkfree/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Thinkfree/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/DeskClockGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/DeskClockGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/DeskClockGoogle/DeskClockGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/DeskClockGoogle/DeskClockGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Phonesky/Phonesky.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Phonesky/Phonesky.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Phonesky/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Phonesky/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/NetworkLocation/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/NetworkLocation/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/NetworkLocation/NetworkLocation.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/NetworkLocation/NetworkLocation.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/PlusOne/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/PlusOne/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/PlusOne/PlusOne.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/PlusOne/PlusOne.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleBackupTransport/GoogleBackupTransport.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleBackupTransport/GoogleBackupTransport.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleBackupTransport/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleBackupTransport/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GooglePartnerSetup/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GooglePartnerSetup/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GooglePartnerSetup/GooglePartnerSetup.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GooglePartnerSetup/GooglePartnerSetup.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleServicesFramework/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleServicesFramework/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleServicesFramework/GoogleServicesFramework.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleServicesFramework/GoogleServicesFramework.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Videos/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Videos/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Videos/Videos.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Videos/Videos.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Street/Street.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Street/Street.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/Street/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/Street/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/EmailGoogle/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/EmailGoogle/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/EmailGoogle/EmailGoogle.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/EmailGoogle/EmailGoogle.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/YouTube/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/YouTube/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/YouTube/YouTube.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/YouTube/YouTube.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleTTS/GoogleTTS.apk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleTTS/GoogleTTS.apk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/app/GoogleTTS/Android.mk ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/app/GoogleTTS/Android.mk
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/bin/fRom ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/bin/fRom
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjcon.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjcon.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjcon.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjcon.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libext2fs.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libext2fs.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjubase2.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjubase2.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/lib_dic_en_xlarge_USUK.conf.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/lib_dic_en_xlarge_USUK.conf.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libext2_uuid.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libext2_uuid.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libvoicesearch.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libvoicesearch.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjubase1gb.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjubase1gb.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnfc_jni.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnfc_jni.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/lib_dic_en_USUK.conf.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/lib_dic_en_USUK.conf.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjubase1.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjubase1.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libsecril-client.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libsecril-client.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjubase2.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjubase2.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libEnjemailuri.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libEnjemailuri.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libiwnn.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libiwnn.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libext2_profile.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libext2_profile.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjubase1.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjubase1.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libfilterpack_facedetect.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libfilterpack_facedetect.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjtan.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjtan.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libflint_engine_jni_api.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libflint_engine_jni_api.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjyomi.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjyomi.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libearthmobile.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libearthmobile.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjexyomi.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjexyomi.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnfc.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnfc.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/lib_dic_morphem_ja_JP.conf.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/lib_dic_morphem_ja_JP.conf.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjname.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjname.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libfrsdk.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libfrsdk.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjaddress.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjaddress.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libpicowrapper.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libpicowrapper.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libspeexwrapper.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libspeexwrapper.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/lib_dic_ja_JP.conf.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/lib_dic_ja_JP.conf.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjexyomi_re.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjexyomi_re.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjubase1us.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjubase1us.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libext2_blkid.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libext2_blkid.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libennjubase3.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libennjubase3.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libfacelock_jni.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libfacelock_jni.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libvideochat_stabilize.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libvideochat_stabilize.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/lib_dic_ja_xlarge_JP.conf.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/lib_dic_ja_xlarge_JP.conf.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libkaomoji_tyukyu.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libkaomoji_tyukyu.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libmicrobes_jni.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libmicrobes_jni.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjexyomi_new.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjexyomi_new.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libkaomoji_kihon.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libkaomoji_kihon.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjemoji.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjemoji.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libnjfzk.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libnjfzk.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libext2_e2p.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libext2_e2p.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libext2_com_err.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libext2_com_err.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libvideochat_jni.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libvideochat_jni.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libWVphoneAPI.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libWVphoneAPI.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/lib/libgcomm_jni.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/lib/libgcomm_jni.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/etc/fallback_fonts.xml ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/etc/fallback_fonts.xml
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/etc/sirfgps.conf ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/etc/sirfgps.conf
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/etc/smc_normal_world_android_cfg.ini ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/etc/smc_normal_world_android_cfg.ini
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/full_model.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/full_model.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/firmware/smc_pa_wvdrm.ift ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/firmware/smc_pa_wvdrm.ift
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/firmware/libpn544_fw.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/firmware/libpn544_fw.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/firmware/ducati-m3.bin ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/firmware/ducati-m3.bin
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/firmware/bcm4330.hcd ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/firmware/bcm4330.hcd
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/libWVStreamControlAPI_L1.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/libWVStreamControlAPI_L1.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/libsec-ril.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/libsec-ril.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/libwvdrm_L1.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/libwvdrm_L1.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/libinvensense_mpl.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/libinvensense_mpl.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/hw/gps.omap4.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/hw/gps.omap4.so
cp -a ${JCROM_SC04D_BIN}/docomo/sc04d/proprietary/system/vendor/lib/libwvm.so ${JCROM_ANDROID_ROOT}/vendor/docomo/sc04d/proprietary/system/vendor/lib/libwvm.so

