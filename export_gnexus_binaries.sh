#!/bin/sh
if [ $# -ne 2 ]; then
   echo "example usage: $0 \$ANDROID_ROOT ~/gnexus_binaries"
   exit 1
fi

JCROM_ANDROID_ROOT=$1
JCROM_GNEXUS_BIN=$2

mkdir -p ${JCROM_GNEXUS_BIN}/broadcom/maguro/proprietary
mkdir -p ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary
mkdir -p ${JCROM_GNEXUS_BIN}/samsung/maguro/proprietary

cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/maguro/device-maguro.mk ${JCROM_GNEXUS_BIN}/broadcom/maguro/device-maguro.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/maguro/proprietary/bcm4330.hcd ${JCROM_GNEXUS_BIN}/broadcom/maguro/proprietary/bcm4330.hcd
cp -a ${JCROM_ANDROID_ROOT}/vendor/broadcom/maguro/BoardConfigMaguro.mk ${JCROM_GNEXUS_BIN}/broadcom/maguro/BoardConfigMaguro.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/device-maguro.mk ${JCROM_GNEXUS_BIN}/imgtec/maguro/device-maguro.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libpvrANDROID_WSEGL.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libpvrANDROID_WSEGL.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libEGL_POWERVR_SGX540_120.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libEGL_POWERVR_SGX540_120.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libGLESv2_POWERVR_SGX540_120.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libGLESv2_POWERVR_SGX540_120.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libPVRScopeServices.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libPVRScopeServices.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libsrv_init.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libsrv_init.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/pvrsrvinit ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/pvrsrvinit
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libusc.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libusc.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libIMGegl.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libIMGegl.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libsrv_um.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libsrv_um.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/gralloc.omap4.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/gralloc.omap4.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libglslcompiler.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libglslcompiler.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libGLESv1_CM_POWERVR_SGX540_120.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libGLESv1_CM_POWERVR_SGX540_120.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/proprietary/libpvr2d.so ${JCROM_GNEXUS_BIN}/imgtec/maguro/proprietary/libpvr2d.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/imgtec/maguro/BoardConfigMaguro.mk ${JCROM_GNEXUS_BIN}/imgtec/maguro/BoardConfigMaguro.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/device-maguro.mk ${JCROM_GNEXUS_BIN}/samsung/maguro/device-maguro.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/BoardConfigVendor.mk ${JCROM_GNEXUS_BIN}/samsung/maguro/BoardConfigVendor.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/proprietary/fRom ${JCROM_GNEXUS_BIN}/samsung/maguro/proprietary/fRom
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/proprietary/libsecril-client.so ${JCROM_GNEXUS_BIN}/samsung/maguro/proprietary/libsecril-client.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/proprietary/libsec-ril.so ${JCROM_GNEXUS_BIN}/samsung/maguro/proprietary/libsec-ril.so
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/BoardConfigMaguro.mk ${JCROM_GNEXUS_BIN}/samsung/maguro/BoardConfigMaguro.mk
cp -a ${JCROM_ANDROID_ROOT}/vendor/samsung/maguro/device-vendor.mk ${JCROM_GNEXUS_BIN}/samsung/maguro/device-vendor.mk

