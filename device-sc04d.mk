ifeq ($(TARGET_PRODUCT),full_sc04d)

LOCAL_PATH := vendor/docomo/sc04d

PRODUCT_PACKAGES += \
	CalendarGoogle \
	CellBroadcastReceiver \
	ChromeBookmarksSyncAdapter \
	DeskClockGoogle \
	EmailGoogle \
	ExchangeGoogle \
	FaceLock \
	GalleryGoogle \
	GenieWidget \
	Gmail \
	GoogleBackupTransport \
	GoogleContactsSyncAdapter \
	GoogleEarth \
	GoogleFeedback \
	GoogleLoginService \
	GooglePartnerSetup \
	GoogleQuickSearchBox \
	GoogleServicesFramework \
	GoogleTTS \
	GMS_Maps \
	LatinImeDictionaryPack \
	LatinImeGoogle \
	MediaUploader \
	MiniDcmWapPushHelper \
	Music2 \
	NetworkLocation \
    Nfc \
	OneTimeInitializer \
	Phonesky \
	PlusOne \
	SetupWizard \
	Street \
	TagGoogle \
	Talk \
	Thinkfree \
	VideoEditorGoogle \
	Videos \
	VoiceSearch \
	YouTube \
	iWnnIME2_No_Flick \
	spmode_mail_downloader \
	talkback

PRODUCT_PACKAGES += \
	PhaseBeam \
	Galaxy4 \
	NoiseField

PRODUCT_PACKAGES += \
	JCBeam

#	HoloSpiral \

### Add
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/lib/libmicrobes_jni.so:system/lib/libmicrobes_jni.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/hw/gps.omap4.so:system/vendor/lib/hw/gps.omap4.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libWVStreamControlAPI_L1.so:system/vendor/lib/libWVStreamControlAPI_L1.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libinvensense_mpl.so:system/vendor/lib/libinvensense_mpl.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libwvdrm_L1.so:system/vendor/lib/libwvdrm_L1.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libwvm.so:system/vendor/lib/libwvm.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so:system/vendor/lib/drm/libdrmwvmplugin.so

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/features.xml:system/etc/permissions/features.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.samsung.device.xml:system/etc/permissions/com.samsung.device.xml \
	$(LOCAL_PATH)/proprietary/system/etc/spn-conf.xml:system/etc/spn-conf.xml \
	$(LOCAL_PATH)/proprietary/system/etc/voicemail-conf.xml:system/etc/voicemail-conf.xml \
	$(LOCAL_PATH)/proprietary/system/etc/updatecmds/google_generic_update.txt:system/etc/updatecmds/google_generic_update.txt

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar \
	$(LOCAL_PATH)/proprietary/system/framework/com.google.android.media.effects.jar:system/framework/com.google.android.media.effects.jar \
	$(LOCAL_PATH)/proprietary/system/framework/com.google.widevine.software.drm.jar:system/framework/com.google.widevine.software.drm.jar \
	$(LOCAL_PATH)/proprietary/system/framework/com.samsung.device.jar:system/framework/com.samsung.device.jar

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/lib/libfacelock_jni.so:system/lib/libfacelock_jni.so \
	$(LOCAL_PATH)/proprietary/system/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
	$(LOCAL_PATH)/proprietary/system/lib/libfrsdk.so:system/lib/libfrsdk.so \
	$(LOCAL_PATH)/proprietary/system/lib/libvoicesearch.so:system/lib/libvoicesearch.so

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/bin/fRom:system/bin/fRom \
	$(LOCAL_PATH)/proprietary/system/lib/libsecril-client.so:system/lib/libsecril-client.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libsec-ril.so:system/vendor/lib/libsec-ril.so

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/media/LMprec_508.emd:system/media/LMprec_508.emd \
	$(LOCAL_PATH)/proprietary/system/media/PFFprec_600.emd:system/media/PFFprec_600.emd \
	$(LOCAL_PATH)/proprietary/system/media/bootanimation.zip:system/media/bootanimation.zip \
	$(LOCAL_PATH)/proprietary/system/media/video/AndroidInSpace.240p.mp4:system/media/video/AndroidInSpace.240p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/AndroidInSpace.480p.mp4:system/media/video/AndroidInSpace.480p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Disco.240p.mp4:system/media/video/Disco.240p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Disco.480p.mp4:system/media/video/Disco.480p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Sunset.240p.mp4:system/media/video/Sunset.240p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Sunset.480p.mp4:system/media/video/Sunset.480p.mp4

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/fonts/MTLmr3m.ttf:system/fonts/MTLmr3m.ttf

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/full_model.bin:system/vendor/pittpatt/models/recognition/face.face.y0-y0-22-b-N/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/right_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rn7-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/nose_base-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-r0-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin:system/vendor/pittpatt/models/detection/multi_pose_face_landmark_detectors.3/left_eye-y0-yi45-p0-pi45-rp7-ri20.2d_n2/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/full_model.bin:system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rp30-ri30.5/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/full_model.bin:system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-rn30-ri30.5/full_model.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/full_model.bin:system/vendor/pittpatt/models/detection/yaw_roll_face_detectors.3/head-y0-yi45-p0-pi45-r0-ri30.4a/full_model.bin

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/vendor/firmware/libpn544_fw.so:system/vendor/firmware/libpn544_fw.so \
	$(LOCAL_PATH)/proprietary/system/vendor/firmware/bcm4330.hcd:system/vendor/firmware/bcm4330.hcd \
	$(LOCAL_PATH)/proprietary/system/vendor/firmware/ducati-m3.bin:system/vendor/firmware/ducati-m3.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/firmware/smc_pa_wvdrm.ift:system/vendor/firmware/smc_pa_wvdrm.ift

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/vendor/etc/fallback_fonts.xml:system/vendor/etc/fallback_fonts.xml \
	$(LOCAL_PATH)/proprietary/system/vendor/etc/sirfgps.conf:system/vendor/etc/sirfgps.conf \
	$(LOCAL_PATH)/proprietary/system/vendor/etc/smc_normal_world_android_cfg.ini:system/vendor/etc/smc_normal_world_android_cfg.ini

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/lib/libEnjemailuri.so:system/lib/libEnjemailuri.so \
	$(LOCAL_PATH)/proprietary/system/lib/lib_dic_en_USUK.conf.so:system/lib/lib_dic_en_USUK.conf.so \
	$(LOCAL_PATH)/proprietary/system/lib/lib_dic_en_xlarge_USUK.conf.so:system/lib/lib_dic_en_xlarge_USUK.conf.so \
	$(LOCAL_PATH)/proprietary/system/lib/lib_dic_ja_JP.conf.so:system/lib/lib_dic_ja_JP.conf.so \
	$(LOCAL_PATH)/proprietary/system/lib/lib_dic_ja_xlarge_JP.conf.so:system/lib/lib_dic_ja_xlarge_JP.conf.so \
	$(LOCAL_PATH)/proprietary/system/lib/lib_dic_morphem_ja_JP.conf.so:system/lib/lib_dic_morphem_ja_JP.conf.so \
	$(LOCAL_PATH)/proprietary/system/lib/libearthmobile.so:system/lib/libearthmobile.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjcon.so:system/lib/libennjcon.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjubase1.so:system/lib/libennjubase1.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjubase1gb.so:system/lib/libennjubase1gb.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjubase1us.so:system/lib/libennjubase1us.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjubase2.so:system/lib/libennjubase2.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjubase3.so:system/lib/libennjubase3.so \
	$(LOCAL_PATH)/proprietary/system/lib/libennjyomi.so:system/lib/libennjyomi.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_blkid.so:system/lib/libext2_blkid.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_com_err.so:system/lib/libext2_com_err.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_e2p.so:system/lib/libext2_e2p.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_profile.so:system/lib/libext2_profile.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_uuid.so:system/lib/libext2_uuid.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2fs.so:system/lib/libext2fs.so \
	$(LOCAL_PATH)/proprietary/system/lib/libflint_engine_jni_api.so:system/lib/libflint_engine_jni_api.so \
	$(LOCAL_PATH)/proprietary/system/lib/libiwnn.so:system/lib/libiwnn.so \
	$(LOCAL_PATH)/proprietary/system/lib/libkaomoji_kihon.so:system/lib/libkaomoji_kihon.so \
	$(LOCAL_PATH)/proprietary/system/lib/libkaomoji_tyukyu.so:system/lib/libkaomoji_tyukyu.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjaddress.so:system/lib/libnjaddress.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjcon.so:system/lib/libnjcon.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjemoji.so:system/lib/libnjemoji.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjexyomi.so:system/lib/libnjexyomi.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjexyomi_new.so:system/lib/libnjexyomi_new.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjexyomi_re.so:system/lib/libnjexyomi_re.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjfzk.so:system/lib/libnjfzk.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjname.so:system/lib/libnjname.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjtan.so:system/lib/libnjtan.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjubase1.so:system/lib/libnjubase1.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnjubase2.so:system/lib/libnjubase2.so \
	$(LOCAL_PATH)/proprietary/system/lib/libpicowrapper.so:system/lib/libpicowrapper.so \
	$(LOCAL_PATH)/proprietary/system/lib/libspeexwrapper.so:system/lib/libspeexwrapper.so \
	$(LOCAL_PATH)/proprietary/system/lib/libvideochat_jni.so:system/lib/libvideochat_jni.so \
	$(LOCAL_PATH)/proprietary/system/lib/libvideochat_stabilize.so:system/lib/libvideochat_stabilize.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnfc.so:system/lib/libnfc.so \
	$(LOCAL_PATH)/proprietary/system/lib/libnfc_jni.so:system/lib/libnfc_jni.so \
	$(LOCAL_PATH)/proprietary/system/lib/libWVphoneAPI.so:system/lib/libWVphoneAPI.so \
	$(LOCAL_PATH)/proprietary/system/lib/libgcomm_jni.so:system/lib/libgcomm_jni.so

PRODUCT_PROPERTY_OVERRIDES += \
	ro.com.google.gmsversion=4.0_r2

$(call inherit-product-if-exists, vendor/imgtec/maguro/device-maguro.mk)

endif

