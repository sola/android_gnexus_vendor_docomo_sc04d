ifeq ($(TARGET_PRODUCT),full_sc04d)
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE := spmode_mail_downloader
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := spmode_mail_downloader
LOCAL_SRC_FILES := spmode_mail_downloader.apk

include $(BUILD_PREBUILT)
endif
